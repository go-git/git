FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > git.log'

COPY git .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' git
RUN bash ./docker.sh

RUN rm --force --recursive git
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD git
